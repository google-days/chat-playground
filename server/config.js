const fromEnv = (key, required) => {
  const value = process.env[key];
  if (value === undefined) {
    if (required) throw new Error('Missing environment variable: ', key);
    return null;
  }
  return value;
};

module.exports = {
  redis: {
    host: fromEnv('REDIS_HOST'),
    port: fromEnv('REDIS_PORT'),
  },
};
