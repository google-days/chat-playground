require('dotenv').config();
const express = require('express');
const app = express();
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const config = require('./config');
const { Chat } = require('./socketApi/chat');

if (process.env.SOCKET_ADAPTER === 'redis') {
  const redisAdapter = require('socket.io-redis');
  io.adapter(redisAdapter(config.redis));
}

app.use(require('helmet')());
app.use(require('cors')());
app.use(require('body-parser').json());
app.use(express.static('./public'));
app.post('/dialogflow-webhook', (req, res) => {
  console.log('Received webhook', req.body);
  res.json({
    fulfillment_text: 'Answering through a webhook',
  });
});

Chat(io);

http.listen(3000, () => console.log('Listening...'));
