const dialogFlowService = new (require('../lib/dialogflow.service'))('frontmen-eindhoven');
const uuid = require('uuid');

const sessions = new Map();

function Chat(io) {
  io.on('connection', socket => {
    const session = new ChatSession(socket, () => {
      sessions.delete(session.id);
    });
    sessions.set(session.id, session);
  });
}

function onWebhook(sessionPath) {
  const sessionId = sessionPath.split('/').slice(-1)[0];
  if (!sessions.has(sessionId)) return false;
  const session = sessions.get(sessionId);
  console.log('Found session, sending message');
  session.send('Received a webhook!');
  return true;
}

class ChatSession {
  constructor(socket, disconnectCb) {
    this.socket = socket;
    this.id = uuid.v4();
    this.disconnectCb = disconnectCb;
    console.log('ChatSession started', this.id);
    socket.on('diconnect', () => this.onDisconnect());
    socket.on('chat', msg => this.onChat(msg));
  }
  onDisconnect() {
    console.log('ChatSession stopped', this.id);
    this.disconnectCb();
  }
  send(message) {
    this.socket.emit('chat', {
      user: 'Bot',
      message: message,
      id: 'bot-' + Date.now(),
    });
  }
  onChat({ user, message, id }) {
    const msg = { user, message, id };
    console.log('Chat received', this.id, msg);

    // echo the chat
    this.socket.emit('chat', msg);

    dialogFlowService.detectIntent(this.id, message).then(result => {
      if (result) {
        this.send(result.fulfillmentText);
      }
    });
  }
}

module.exports = {
  Chat,
  onWebhook,
};
