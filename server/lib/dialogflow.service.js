const dialogflow = require('dialogflow');

class DialogFlowService {
  constructor(projectId) {
    this.projectId = projectId;
  }
  /**
   * Send a query to the dialogflow agent, and return the query result.
   * @param {string} sessionId A unique user session id
   * @param {string} text The message to parse
   * @returns {Object<Intent>|null} Intent or null if not matched
   */
  async detectIntent(sessionId, text) {
    // Create a new session
    const sessionClient = new dialogflow.SessionsClient();
    const sessionPath = sessionClient.sessionPath(this.projectId, sessionId);

    // The text query request.
    const request = {
      session: sessionPath,
      queryInput: {
        text: {
          // The query to send to the dialogflow agent
          text,
          // The language used by the client (en-US)
          languageCode: 'nl-NL',
        },
      },
    };

    // Send request and log result
    const responses = await sessionClient.detectIntent(request);
    console.log('Detected intent');
    const result = responses[0].queryResult;
    console.log(`  Query: ${result.queryText}`);
    console.log(`  Response: ${result.fulfillmentText}`);
    if (result.intent) {
      console.log(`  Intent: ${result.intent.displayName}`);
      return result;
    } else {
      console.log(`  No intent matched.`);
      return null;
    }
  }
}

module.exports = DialogFlowService;
