FROM node:10-alpine

RUN apk --no-cache add git bash
RUN apk add --no-cache --repository=http://dl-cdn.alpinelinux.org/alpine/edge/community gitlab-runner
RUN apk --no-cache add docker

RUN echo "#!/bin/bash" >> /usr/bin/kubectl && \
    echo 'echo kubectl stubbed' >> /usr/bin/kubectl && \
    chmod +x /usr/bin/kubectl
WORKDIR /app

ENTRYPOINT ["gitlab-runner", "exec", "shell"]
