#!/usr/bin/env node

const { promisify } = require('util');
const { exec, spawn } = require('child_process');
const fs = require('fs');
const path = require('path');
const readline = require('readline');

const rootDir = path.resolve(__dirname, '..');
const newlines = /(\n|\r\n)/g;
const rl = readline.createInterface({ input: process.stdin, output: process.stdout });

const ask = q => new Promise(resolve => rl.question(q, resolve));
const sh = (...args) => promisify(exec)(...args).then(({ stdout }) => stdout.split(newlines));
const runInTTY = command =>
  new Promise((resolve, reject) => {
    console.log(command);
    const [cmd, ...args] = command.split(/\s+/g);
    const proc = spawn(cmd, args, { stdio: 'inherit' });
    proc.on('close', code => (code === 0 ? resolve() : reject()));
  });
const readdir = promisify(fs.readdir);
const readFile = promisify(fs.readFile);
const stat = promisify(fs.stat);
const isDir = path => stat(path).then(stats => stats.isDirectory(), () => false);

const not = fn => (...args) => !fn(...args);
const isIndented = str => str.trimLeft() !== str;
const oneOf = values => value => values.includes(value);
const empty = value => !value;
const startsWith = str => value => value.trimLeft().startsWith(str);
const slice = (left, right) => str => str.slice(left, right);

const getRepos = async () => {
  const items = await readdir(rootDir);
  const repos = ['.'];
  for (let item of items) {
    try {
      const isRepo = await isDir(path.resolve(rootDir, item, '.git'));
      if (isRepo) repos.push(item);
    } catch (err) {
      // noop
    }
  }
  return repos;
};

const getJobs = async repo => {
  const filename = path.resolve(rootDir, repo, '.gitlab-ci.yml');
  const gitlabCIFile = await readFile(filename, { encoding: 'utf8' });
  return gitlabCIFile
    .split(newlines)
    .filter(not(empty))
    .filter(not(isIndented))
    .filter(not(startsWith('.')))
    .filter(not(startsWith('#')))
    .map(slice(0, -1))
    .filter(not(oneOf(['variables', 'stages'])));
};

const buildRuntime = () =>
  runInTTY(`docker build -t ci-debug -f ${rootDir}/util/ci-debug.Dockerfile ${rootDir}/util`);

(async function() {
  let build = false;
  try {
    await sh(`docker images | grep ci-debug`);
  } catch (error) {
    build = true;
  }
  if (process.argv[2] === 'rebuild') {
    build = true;
  }
  try {
    if (build) await buildRuntime();
    const repos = await getRepos();
    if (repos.length > 1) {
      repos.forEach((repo, i) => {
        console.log(`${i}) ${repo}`);
      });
    }
    let repoIndex = repos.length > 1 ? null : 0;
    while (!repos[repoIndex]) repoIndex = await ask(`[0-${repos.length - 1}] Which repo to test? `);
    const repo = repos[repoIndex];

    const jobs = await getJobs(repo);
    jobs.forEach((job, i) => {
      console.log(`${i}) ${job}`);
    });
    let jobIndex;
    while (!jobs[jobIndex]) jobIndex = await ask(`[0-${jobs.length - 1}] Which job to test? `);
    const job = jobs[jobIndex];

    await sh(`touch ${rootDir}/ci.env`);
    const cmd = `docker run --rm --interactive --tty --env-file ${rootDir}/ci.env --volume ${rootDir}/${repo}:/app --volume /var/run/docker.sock:/var/run/docker.sock ci-debug ${job}`;
    await runInTTY(cmd);
    process.exit(0);
  } catch (err) {
    console.error(err);
    process.exit(1);
  }
})();
