import io from 'socket.io-client';

const socket = io();

const noop = () => null;
const chatListeners = [];
let userListener = noop;
let user = 'TEST';

socket.on('chat', msg => {
  chatListeners.forEach(fn => fn(msg));
});

socket.on('users', users => userListener(users));

const listen = listener => {
  chatListeners.push(listener);
  const sender = message =>
    socket.emit('chat', {
      user,
      message,
      id: `${user}-${Date.now()}`,
    });
  sender.destroy = () => {
    const index = chatListeners.indexOf(listener);
    chatListeners.splice(index, 1);
  };
  return sender;
};

const join = alias => {
  user = alias;
};

export default {
  listen,
  join,
};
