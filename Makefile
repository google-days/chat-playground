DEFAULT := local

clean:
	@docker-compose down --volumes
	@rm -Rf {node_modules,public}

rebuild:
	@docker-compose build --no-cache

docker:
	@docker-compose up

local:
	@yarn install
	@yarn start

ci:
	@./util/ci-debug.js
