FROM node:10-alpine AS develop

WORKDIR /app
COPY package.json .
COPY yarn.lock .

RUN yarn install

CMD yarn start

FROM node:10-alpine AS production

WORKDIR /app
COPY  --from=develop /app .
COPY . .

RUN yarn run build-prod

CMD node server/index